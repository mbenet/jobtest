job_test (Marc Benet)  README FILE
==================================

A Symfony project created on April 1, 2015, 7:25 pm.

Bitbucket repository URL
https://bitbucket.org/mbenet/jobtest.git


Backend decisions
-> I've used Symfony2 v2.6.
-> I've generated a File entity and a Folder entity
-> Both entities have an 1:n relation with Folder, because any of those entities can be inside a folder.
-> All the entities have been mapped to be deployed using Doctrine into a PostgreSql database.
-> You will be able to find all my valid code into <PROJECTFOLDER>/Marc/JobTestBundle/.

Frontend decisions
-> I don't have enough knowledge about AngularJS to use it in that scenario.
-> I've used external libraries such as jQuery and a javascript plugin called DataTables in order to offer you a better display of the table.
-> Symfony has an own tool for doing basic unit tests, you will find a simple one in the folder <PROJECTFOLDER>/Marc/JobTestBundle/Tests/Controller/.
-> I've extended the main template twice, one time on the root of the bundle (where I introduced the external libraries) and the other one on the controller's view in order to build the final display.

