<?php

namespace Marc\JobTestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function showFolderContentAction($folderId)
    {
    	//gets the id obtained by get and searches for the folder data
    	$rootData = $this->getDoctrine()->getManager()->getRepository('MarcJobTestBundle:Folder')->findOneById($folderId);
    	//gets the breadcrumb for the view
    	$breadcrumbData = $this->get('marc_breadcrumb')->getBreadCrumbFromFolder($folderId);
    	//gets the content of root
    	$rootFolderContent = $this->getDoctrine()->getManager()->getRepository('MarcJobTestBundle:Folder')->findByFolder($rootData);
    	$rootFilesContent = $this->getDoctrine()->getManager()->getRepository('MarcJobTestBundle:File')->findByFolder($rootData);

        return $this->render('MarcJobTestBundle:Default:index.html.twig', array('rootFolderContent' => $rootFolderContent, 
        																		'rootFilesContent'  => $rootFilesContent, 
        																		'breadcrumbData' 	=> $breadcrumbData));
    }
}
