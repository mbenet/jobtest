<?php

namespace Marc\JobTestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * File
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class File
{
    /**
     * @var bigint 
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="file_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation", type="datetime")
     */
    private $creation;

    /**
     * @ORM\Column(type="integer")
     * @ORM\ManyToOne(targetEntity="Marc\JobTestBundle\Entity\Folder",inversedBy="id")
     * @ORM\JoinColumn(name="folder_id", referencedColumnName="id")
     */
    private $folder;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return File
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     * @return File
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime 
     */
    public function getCreation()
    {
        return $this->creation;
    }

    /**
     * Set folder
     *
     * @param Marc\JobTestBundle\Entity\Folder $folder
     * @return Folder
     */
    public function setFolder(Marc\JobTestBundle\Entity\Folder $folder)
    {
        $this->folder = $folder;
    }

    /**
     * Get folder
     *
     * @return Marc\JobTestBundle\Entity\Folder 
     */
    public function getFolder()
    {
        return $this->folder;
    }
}
