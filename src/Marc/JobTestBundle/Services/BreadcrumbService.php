<?php

namespace Marc\JobTestBundle\Services;

use Doctrine\ORM\EntityManager;

class BreadcrumbService 
{
	protected $em = null;

	public function __construct(EntityManager $em) {
		$this->em = $em;
	}

	/**
	 * 
	 * Returns an array with all the breadcrumb nodes obtained from the folder id
	 * @param integer $folderId
	 * @return array $breadcrumbNodes
	 */
	public function getBreadCrumbFromFolder($folderId)
	{
		//initializes the array
		$breadcrumbNodes = array();
		//gets the entity data
		$folderEntity = $this->em->getRepository('MarcJobTestBundle:Folder')->findOneById($folderId);
		
		//iterates through all the linked nodes untill the root one
		while ($folderEntity->getId() != 0) {
			//keeps the node
			$breadcrumbNodes[] = array( 'name' => $folderEntity->getName(), 'id' => $folderEntity->getId() );

			//gets the containing folder
			$folderEntity = $this->em->getRepository('MarcJobTestBundle:Folder')->findOneById($folderEntity->getFolder());
		}
		
		//at the end inserts the root node
		$breadcrumbNodes[] = array( 'name' => $folderEntity->getName(), 'id' => $folderEntity->getId() );

		//before reordering the array the actual folder is flagged
		$breadcrumbNodes[0]['id'] = 'actual';
		//at the end we need to ordenate the array in an inverse mode in order to get allways first the root node
		krsort($breadcrumbNodes);

		return $breadcrumbNodes;
	}
}